import json
from neo4j import GraphDatabase

# neo4j = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "histovec"), encrypted=False)


with open("qualif_process_siv_full.json", "r") as fh:
    input_json = json.load(fh)


def find_parent_field(input_json, field_table, field_name, found, iter):
    # search in source field
    if input_json[field_table]['depends_on_source']:
        sources = []
        for source in input_json[field_table]['depends_on_source']:
            sources.append((source, field_name))
        print('source --> ', sources)

        return True
    for internal in input_json[field_table]['depends_on_internal']:
        if not found:
            if field_name in input_json[internal]['output']:
                print(internal, field_name)
                if iter < max_iter:
                    found = find_parent_field(input_json, internal, field_name, False, iter +1)
                else:
                    print("sstop itering")


max_iter = 10
for final_field in input_json["SIV_API"]['output']:
    if final_field != 'dos_id':
        print('------- ', final_field, ' --------')
        find_parent_field(input_json, "SIV_API", final_field, False, 1)
