import json
from neo4j import GraphDatabase

neo4j = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "histovec"), encrypted=False)


with open("qualif_process_siv_full.json", "r") as fh:
    input_json = json.load(fh)


for table in input_json:
    for internal in input_json[table]['depends_on_internal']:
        cypher = "MERGE (a:internal{table_name:'" + internal + "'})"
        with neo4j.session() as session:
            session.run(cypher)
        cypher = "match (a:internal{table_name:'" + table + "'}), (b:internal{table_name:'" + internal + "'}) MERGE (a)-[r:DEPENDS_ON_TABLE]->(b) return r"
        print(cypher)
        with neo4j.session() as session:
            session.run(cypher)
    for source in input_json[table]['depends_on_source']:
        cypher = "MERGE (a:source{table_name:'" + source + "'})"
        with neo4j.session() as session:
            session.run(cypher)
        cypher = "match (a:internal{table_name:'" + table + "'}), (b:source{table_name:'" + source + "'}) MERGE (a)-[r:DEPENDS_ON_TABLE]->(b) return r"
        print(cypher)
        with neo4j.session() as session:
            session.run(cypher)