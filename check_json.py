import json
import argparse
import os

searched_op = (
"CUMUL_OPERATIONS", "IMMAT_DIPLO_DEMANDE", "IMMAT_DIPLO_CONFIRMATION", "IMMAT_DIPLO_INFIRMATION", "IMMAT_DIPLO_RETIRER",
"IMMAT_DIPLO_PREM_VO_CONF", "IMMAT_NORMALE_PREM_VO", "IMMAT_NORMALE", "IVN_CARROSSIER_QUALIFIE", "IMMAT_WG",
"IMMAT_PROVISOIRE", "RENOUV_WG",
"PROROG_IMMAT", "CHANG_TIT_NORMAL", "CHANG_TIT_PREDEMANDE_INTERNET", "CHANG_TIT_DIPLO", "CHANG_LOC", "MODIFIER_USAGES",
"FIN_USAGE_DEMO_VENTE", "FIN_USAGE_DEMO_FINDEM", "PROROG_USAGE", "ATT_NUM_USURPATION", "MODIF_ADRESSE",
"MODIF_ADRESSE_DGME", "MODIF_ADRESSE_LOCATAIRE_PRO", "MODIF_ETAT_CIVIL",
"REIMMAT_ETRANGER", "RETOUR_REIMMAT_ETR", "RETRAIT_VOLONTAIRE", "REMISE_CIRC_RETRAIT_VOL", "SORTIE_TERRITOIRE",
"REMISE_CIRC_SORTIE_TERR", "DUPLICATA", "DEC_VE", "PREM_RAP_VE", "SEC_RAP_VE", "INSCRIPTION_GAGE", "CESSION_GAGE",
"RADIATION_GAGE", "PROROG_GAGE", "DECLARATION_DESTRUCTION", "DESTRUCTION_VEHICULE", "DECLARATION_ACHAT",
"DECLARATION_CESSION", "DECLARATION_CESSION_DGME", "INSCRIRE_OTCI", "LEVER_OTCI", "INSCRIRE_OPPOSITION",
"LEVER_OPPOSITION", "INSCRIRE_DVS", "LEVER_DVS", "RENOUV_DVS", "RELANCE_PROD_DOC", "PROD_TITRE_HORS_DUPLICATA",
"RESTITUTION_TITRE", "DECLARATION_PERTE_TITRE", "EDITION_CPI", "MODIF_CARAC_TECH", "REMISE_LOT_TITRE",
"REMISE_TITRE_FO", "REMISE_TITRE_PREF", "IMMOBILISATION_VEHICULE",
"IMMOBILISATION_VEHICULE_ADM", "LEVEE_IMMOBILISATION_VEHICULE", "INSCRIRE_MSG_SERVICE", "INCONU_REPRISE",
"REPRISE_DP7_1993", "CORRECTION_DOSSIER", "CONVERSION_DOSSIER_FNI", "CORRECTION_SIT_ADMIN", "CORRECTION_TITULAIRE",
"CORRECTION_VEHICULE", "CORRECTION_TITRE", "CORRECTION_AUTORISATION", "PROD_DOC", "INSCRIRE_OVE", "LEVER_OVE",
"MODIF_DROIT_OPPOSITION", "ATTRIBUTION_CODE_CONF", "VEHICULE_REFCNIT_RUBVARIABLES", "VEHICULE_REFCNIT_MENTIONVARIABLE",
"RECHERCHE_DOSSIERS", "CONSULT_DOSSIER", "ANNULATION_OPERATION", "MAJ_PARAM_FONC", "ADMIN_NOMENC", "CREA_COMPT_UTIL",
"MODIF_COMPT_UTIL", "INVA_COMPT_UTIL", "SUSP_REAC_COMPT_UTIL", "CREA_COMPT_PART", "MODIF_COMPT_PART",
"SUSP_REAC_COMPT_PART", "ACTIVATION_COMPT_PART", "SUSPENSION_COMPT_PART", "RAD_COMPT_PART", "RETRAIT_COMPT_PART",
"RESILIATION_COMPT_PART", "MODIFICATION_NOMBRE_GUICHETS", "CREA_COMPT_CONC", "MODIF_COMPT_CONC", "INVA_COMPT_CONC",
"ANNULATION", "MODIF_ADRESSE_CVN", "CHANG_TIT_NORMAL_CVN", "CHANG_TIT_DIPLO_CVN", "CHANG_LOC_CVN", "PROROG_USAGE_CVN",
"MODIFIER_USAGES_CVN", "MODIF_ETAT_CIVIL_CVN", "MODIF_CARAC_TECH_CVN", "RENOUV_WG_CVN", "FIN_USAGE_DEMO_FINDEM_CVN",
"FIN_USAGE_DEMO_VENTE_CVN", "ATT_NUM_USURPATION_CVN", "ANNULATION_ETATS_ADMINISTRATIFS", "EXECUTION_REQUETE_ANTS",
"ANN_PROG_REQUETE_ANTS", "ENR_PROG_REQUETE_ANTS", "OUTREPASSER_VOL", "OUTREPASSER_VIN_DOUBLON",
"OUTREPASSER_VIN_NON_CONTROLE", "OUTREPASSER_OTC_INDISPONIBLE", "OUTREPASSER_CT_EXONERATION",
"OUTREPASSER_CT_EXEMPTION", "OUTREPASSER_EXONERATION_TAXES", "OUTREPASSER_REFCNIT_RUBVARIABLES",
"OUTREPASSER_REFCNIT_MENTIONVARIABLE", "BATCH_VEHICULES_IMPORTES", "EDIT_ETAT_REGIE"
)


def get_mandatory_keys() -> list:
    """
    Liste des clefs obligatoires
    :return:
    """
    return ["depends_on_internal", "depends_on_source", "scheme", "step", "output"]


def check_mandatory_keys(data: dict) -> str:
    """
    Vérifie si toutes les clefs obligatoires sont présentes
    :param data:
    :return:
    """
    message = ""
    for processing_step in data:
        for key in get_mandatory_keys():
            if key not in data[processing_step].keys():
                message += "%s %s %s \n" % (data[processing_step], processing_step, key)
    if message == "":
        message = "All mandatory keys present"
    return message


def check_internals_descriptions(data: dict) -> str:
    """
    Verifie si toutes les tables demandées en interne sont bien définies
    :param data:
    :return:
    """
    message = ""
    for processing_step in data:
        for internal in data[processing_step]["depends_on_internal"]:
            if internal not in data:
                message += "internal error %s \n" % internal
    if message == "":
        message = 'Internals are described'
    return message


def check_source_description(data: dict) -> str:
    """
    est ce que les sources de données sont bien décrites (internal et sources)
    :param data:
    :return:
    """
    message = ""
    for processing_step in data:
        for internal in data[processing_step]["depends_on_internal"]:
            if internal not in data[processing_step]:
                message += "Missing internal description %s %s \n" % (processing_step, internal)
        for source in data[processing_step]["depends_on_source"]:
            if source not in data[processing_step]:
                message += "Missing source description %s %s \n" % (processing_step, source)
    if message == "":
        message = 'Description keys are present'
    return message


def check_missing_outputs(data: dict) -> str:
    """
    Est-ce que tous les parametres attendus sont bien des outputs de la bonne source ?
    :param data:
    :return:
    """
    message = ""
    for processing_step in data:
        for internal in data[processing_step]['depends_on_internal']:
            for require in data[processing_step][internal]:
                if require not in data[internal]['output']:
                    message += "Missing entry %s %s %s \n" % (processing_step, internal, require)
    if message == "":
        message = "Toutes les entrées sont présentes"
    return message


def get_source_depedencies(data: dict) -> dict:
    """
    Donne toutes les dépendances à des données sources contenues dans le modèle
    :param data:
    :return:
    """

    sources = {}
    for processing_step in data:
        for table in data[processing_step]['depends_on_source']:
            if table not in sources:
                sources[table] = []
            for column in data[processing_step][table]:
                sources[table].append(column)
    for table in sources:
        sources[table] = list(set(sources[table]))
    return sources


def pretty_print_sources(sources: dict, sources_fh=None, screen=False) -> None:
    """
    Affiche ou sauve dans un fichier les dependances aux differentes sources
    :param sources:
    :param sources_fh:
    :param screen:
    :return:
    """
    for table in sources:
        if screen:
            print("%s : %s" % (table, ", ".join(sources[table])))
        if sources_fh is not None:
            sources_fh.write("%s : %s\n" % (table, ", ".join(sources[table])))


def main(filename: str) -> None:
    """
    Main process file
    :param filename:
    :return:
    """
    if os.path.isfile(filename):
        with open("process_siv_full.json", "r") as fh:
            data = json.load(fh)
    else:
        print("File %s does not exist" % filename)
        exit()

    # vérifie la presence des clefs indispensables
    message = check_mandatory_keys(data)
    print(message)

    # Est ce que les tables demandée en internes existent bien ?
    message = check_internals_descriptions(data)
    print(message)

    # Est-ce que les sources sont décrites
    message = check_source_description(data)
    print(message)

    # Est-ce que tous les outputs demandés sont bien définis ?
    message = check_missing_outputs(data)
    print(message)

    # Récupère la liste de toutes les sources utilisées et renvoie dans un fichier
    sources = get_source_depedencies(data)
    sources_desc = open("sources_desc.txt", "w")
    pretty_print_sources(sources, sources_desc)
    sources_desc.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    main(args.filename)
