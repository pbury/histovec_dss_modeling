import json
from datetime import datetime
import re


stats = {}
if True:
    filename  = "logs/histovec-nginx-2021030820_0.log"
    with open(filename, "r") as fh:
        res = re.findall('(\d{8})', filename)
        log_date = res[0]
        if log_date not in stats:
            stats[log_date] = {
                'nb_acheteur': {
                    '200': 0,
                    '201': 0,
                    '404': 0,
                    'xxx': 0,
                    'total': 0
                },
                'nb_vendeur': {
                    '200': 0,
                    '201': 0,
                    '404': 0,
                    'xxx': 0,
                    'total': 0
                }
            }
        for row in fh:
            if re.match(r'^\{.*\}$', row):
                data = json.loads(row)
                if 'content' in data:
                    #print('applicatif ', data)
                    pass
                elif 'backend' in data:
                    h = data['backend']['remote-address']
                    u = '-'
                    server_date = datetime.strptime( data['backend']['server-date'], '%Y-%d-%mT%H:%M:%S.%fZ')
                    t = server_date.strftime('%d/%b/%Y:%H:%M:%S +0000')
                    # jour/mois/année:heure:minutes:secondes zone]
                    r = data['backend']['method'] + ' ' + data['backend']['url'] + ' HTTP/' + data['backend']['http-version']
                    s = str(data['backend']['status-code'])
                    b = str(data['backend']['content-length'])
                    referer = data['backend']['referrer']
                    user_agent = data['backend']['user-agent']
                    matches = re.findall(r'(?P<uuid>\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b)\/(?P<action>[a-zA-Z\-\/]*)', data['backend']['url'])
                    print(matches)
                    #line = h + ' - ' + u + ' [' + t + '] "' + r + '" ' + s + ' ' + b + ' "' + referer + '" "' + user_agent + '"'
print(stats)
